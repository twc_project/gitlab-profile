# The TWC Project

Customizing the Look-and-Feel of Wayland Compositors

 - Goal

   - Create custom compositors via Mix-and-Match of base servers with
     independent window managers.

 - Approach

   - Offload dynamic compositor control to select Wayland clients
     including icons, menus, decorations, taskbar, workspaces,
     shortcuts ...

   - Custom *window manager clients* inter-operate with a base server
     each providing a unique compositor personality.

  - Innovations

    - Window manager clients execute within the server as a "Built-in
      Client" allowing safe sharing of compositor data and state.

    - Window manager clients use a dedicated protocol extension
      containing standardized APIs for delivery of state data and for
      control feedback.

### Summary

The TWC Project is a collection of repositories focused on
extensibility for Wayland compositors with emphasis on Window
Management for Desktop Environments.

There are two key innovations.  The first is the concept of a Wayland
"Built-in Client".  The second is a Wayland protocol extension
specifically designed for Window Management.

Together, they enable the creation of reusable base compositors which
can be selectively combined with any number of custom window managers.
All interactions with a base compositor are by way of well-defined
Wayland interfaces.

A combination of base compositor and custom window manager provides a
unique server look-and-feel.


#### Built-in Clients

A *Built-in Client* is a Wayland client which operates within the
address space and protection domain of a compositor process.  This
means that a window manager for Wayland can be coded *as if* it were
simply a client, but can execute as part of the compositor itself.
All state data can be shared with the window manager, yet stays safely
confined to the server.

#### Window Manager Protocol

A proposed window manager protocol, called the [*Agent Protocol*],
contains interfaces for managing a desktop.  Protocol events enable
server state to be delivered to Built-in Clients in a consistent,
well-defined manner.  Based on user interaction, the window manager
can use Agent requests to alter base compositor operation to the users
liking.

In addition to the Agent Protocol, a window manager may make use of
the Wayland and xdg_shell protocols for imaging and receiving user
input.

By using only the well-known techniques for client/compositor
interaction, a window manager client can be coded to supply a
Graphical User Interface (GUI) within a server for the purpose of
dynamic compositor control.

Related capabilities in the Agent Protocol are partitioned into
various interface objects called *agents*. For example, decorations
and workspaces.  A given client may bind one or more agent objects.
Consequently, a window manager or Desktop Environment might actually
consist of multiple clients.

#### Extensible Compositor Architecture

The API(s) between base compositor and window manager are completely
defined by various Wayland protocols.  All data and control exchanges
occur exclusively through the request/event features of the supported
interfaces.  This arrangement allows the source code for a base
compositor and various window managers to be completely independent.
A base compositor may have many compatible desktops all developed by
unrelated teams.

The following diagram shows a typical arrangement of a base compositor
combined with a window manager.  It is an executable formed from two
code libraries called lib_their_server and lib_my_wm.  Note that
because of the way event callbacks are registered, a plug-in system is
trivial.

```
                        Server Process
     +-----------------------------------------------------+
     |   lib_their_server     *           lib_my_wm        |
     |   ---------------      *           ---------        |
     |                        *                            |
     |                  Agent Protocol                     |
     |   base          <-- requests ---   window manager   |
     |   compositor    ---  events  -->   client           |
     |                        *                            |
     |                        +----------------------------+
     |                        |
     +------------------------+
          ^          ^
         r|e        r|e              <  request|event messaging
          v          v
     +----------+ +---------+
     | terminal | | browser |  .  .  .  external clients
     +----------+ +---------+
  
```

The window manager operates just like any other client.  Unlike a
terminal or browser however, it executes within the server process.

A window manager client can be written by any Wayland practitioner who
is already familiar with protocol extensions.  There's no obscure API
documentation nor any need to touch, or even inspect, base compositor
code.  Third-party window managers are not only possible, but are easy
to integrate.

The variety of server personalities enabled by this approach depends
on the scope and flexibility of the window manager protocol.  The
proposed Agent Protocol has been used to create a
[TWM-style](https://en.wikipedia.org/wiki/Twm) window manager.  The
Agent Protocol is a work-in-progress.

#### Main Repositories

The TWC Project includes the following important repositories:

 - [Agent Protocol] : A Wayland extension protocol for window
   management.

 - [TWC Server Library] : A sample library for creating base
   compositors.  Supports Built-in Clients plus the Wayland,
   xdg_shell, xdg_decoration and Agent protocols.

 - [TWC] : A demonstration base compositor.

 - [Plug-ins] : A collection of basic Built-in Clients plus a window
   manager reminiscent of [CTWM].

These repositories are independent.  E.g., a developer may choose to
only consider the Agent Protocol and disregard the rest.  Another may
choose to use the Agent Protocol and (some) of the plug-ins without
employing the TWC server library.  For exploration purposes, the TWC
Project employs all the repositories.

The provided plug-ins (Built-in Clients) strive to use trivial
graphics to avoid complexity and increase clarity.  Neither the Cairo
nor Pango graphics libraries are used.  A polished look is secondary
to the ideas and concepts.

#### Status

The code base is operational, but is not production ready.  The
Wayland and xdg-shell protocol implementation for Built-in Clients is
incomplete, but is sufficient for the [CTWC] personality to be
working.  Native Wayland clients work as expected.  The Xwayland
subsystem has some issues.  Pull-down menus for some X-clients
(liberoffice) do not behave properly.  Other X-clients (emacs) work
fine.  The TWC server library is based on [Wlroots] version 0.17.4.

#### Building

See the [trial_howto] repository for instructions on how to download,
build and install.  All work is confined to a single directory tree
including installed files.

[*Agent Protocol*]:   https://gitlab.com/twc_project/agent_protocol
[Agent Protocol]:     https://gitlab.com/twc_project/agent_protocol
[TWC Server Library]: https://gitlab.com/twc_project/libtwc
[TWC]:                https://gitlab.com/twc_project/twc
[Plug-ins]:           https://gitlab.com/twc_project/plug-ins
[CTWC]:               https://gitlab.com/twc_project/plug-ins/ctwc
[trial_howto]:        https://gitlab.com/twc_project/trial_howto
[CTWM]:               https://www.ctwm.org/index.html
[Wlroots]:            https://gitlab.freedesktop.org/wlroots/wlroots

#### Acknowledgements

In [\"Thoughts on writing a wayland window manager with wlroots\"](https://inclem.net/2021/04/17/wayland/writing_a_wayland_compositor_with_wlroots/),
Alexander Taylor writes:

> I think ultimately there will naturally end up being multiple active
  compositor projects, some of which support easily creating distinct
  window managers via extensive plugin APIs
